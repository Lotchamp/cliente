
package Cliente;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;


public class Conector extends Thread{
    Socket socket;
    ServerSocket ss;
    BufferedReader entrada;
    DataOutputStream salida;
    int puerto= 2300;
    InputStreamReader entradaSocket;
    
    public Conector(String IP){
         try{
            this.socket = new Socket(IP,puerto);
            
            entradaSocket = new InputStreamReader(socket.getInputStream());
            entrada = new BufferedReader(entradaSocket);
            
            salida= new DataOutputStream(socket.getOutputStream());
            salida.writeUTF("Conectado correctamente\n");
            
        }catch (Exception e){System.out.println("No se conecto nadie");};
    }
    public void run()
    {
        String texto="";
        while(true)
        {try{
            texto = entrada.readLine();
            VentanaCliente.jTextArea1.setText(VentanaCliente.jTextArea1.getText().toString()+"\n"+ texto);
        }catch(IOException e){};
        }
    }
    public void enviarMSG(String mensaje)
    {
        System.out.println("*Enviado*");
        try{
            this.salida = new DataOutputStream(socket.getOutputStream());
            this.salida.writeUTF(mensaje+"\n");
            
            
        }catch (IOException e){
            System.out.println("Hubo un Problema al enviar");
        };
    }
    public String leerMSG()
    {
        try{
            return entrada.readLine();
        }catch(IOException e){};
        
        return null;
    }
    
}
